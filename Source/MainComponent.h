/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback
{
public:
    //==============================================================================
    MainComponent();
    
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage& message) override
    {
        String midiText;
        
        if (message.isNoteOnOrOff())
        {
            midiText << "NoteOn: Channel " << message.getChannel();
            midiText << ":Number" << message.getNoteNumber();
            midiText << ":Velocity" << message.getVelocity();
            
        }
        midiLabel.getTextValue() = midiText;
    }
    
    
    
    //DBG("MIDI Message Received" << MidiMessage);


    ~MainComponent();

    void resized() override;

private:
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
